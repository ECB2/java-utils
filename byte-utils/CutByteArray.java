	public static byte[] cutByteArray(byte[] bytes, int start, int length)
	{
		byte[] out = new byte[length-start];
		for(int i = 0; i < length; i++)
		{
			out[i] = bytes[i+start];
		}
		return out;
	}
