	public static byte[] mergeByteArrays(byte[]... b)
	{
		int newArraySize = 0;
		for(byte[] ba:b)
		{
			newArraySize += ba.length;
		}
		byte[] out = new byte[newArraySize];
		int iterator = 0;
		for(byte[] ba:b)
		{
			for(byte by:ba)
			{
				out[iterator] = by;
				iterator++;
			}
		}
		return out;
	}
